var sections = document.querySelectorAll('section');
var main = document.querySelector('main');

sections.forEach(section => {

  section.addEventListener('click', function() {
    
    sections.forEach(section => {
      section.classList.remove('clicked');
    })
    
    section.classList.add('clicked');

    var sectionRow = section.dataset.row;
    var sectionCol = section.dataset.column;

    if (sectionRow == 1) {
      main.style.gridTemplateRows = '95% 5%'
    } else {
      main.style.gridTemplateRows = '5% 95%'
    }

    if (sectionCol == 1) {
      main.style.gridTemplateColumns = '90% 5% 5%'
    } else if (sectionCol == 2) {
      main.style.gridTemplateColumns = '5% 90% 5%'
    } else {
      main.style.gridTemplateColumns = '5% 5% 90%'
    }

  })
})